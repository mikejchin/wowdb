var wowheadLink = document.querySelector('[data-tracking=wowhead-link]');

if (wowheadLink) {
    window.location = wowheadLink.getAttribute('href');
}
